# -*- coding: utf-8 -*-
import urllib2
import json
import os
from time import sleep
from IRreceiver import *
#os.chdir('/home/pi/Desktop/Avalon')

Server = json.load(open('./ipconfig.json'))['IP']

print '阿瓦隆伺服器 : ' , Server , '\n'
while(True):
    try:
        Out = IR.nextcode()     # non-block , 欲改為block則至IRreceiver.py , 將"set_blocking"函式註解即可
        if(len(Out)!=0):
            print Out[0]
            urllib2.urlopen(Server+"/ir_detected/"+Out[0]).read()
            if(Out[0]=='EQ'):   # 緩衝，避免連按兩次確認
                sleep(0.5)
                IR.nextcode()
    except KeyboardInterrupt:
        print "【關閉】Closing IRreader.py ......"
        break
    except:
        print "【錯誤】伺服器無法連線..."
        pass