# -*- coding: utf-8 -*-
import RPi.GPIO as GPIO
import urllib2
import json
import os
from time import sleep
from DJoke_RC522 import RC522
from buzzer import *

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BCM)

#PIN_BUZZER = 18

# Server IP
Server = json.load(open('./ipconfig.json'))['IP']
    
# RC522
reader = RC522()
players_list = list()
players_dict = dict()


print '阿瓦隆伺服器 : ' , Server , '\n'
while(True):
    try:
        print "已啟動讀卡機，請將卡接近RC522，將會傳送訊息至Server"
        SND_connected()
        while(True):
            id = reader.Wait_for_one()
            content = urllib2.urlopen(Server+"/card_detected/"+str(id)).read()
            SND_cardDetect()
            print 'id:',id
            if(content!=''):
                print '【Beep】'
            else:
                sleep(1)
            while(content=='read_other'):
                id = reader.Wait_for_other()
                content = urllib2.urlopen(Server+"/card_detected/"+str(id)).read()
                print '【Beep】'
                SND_cardDetect()
            
    except KeyboardInterrupt:
        print "【關閉】Closing CardReader.py ......"
        SND_disconnected()
        GPIO.cleanup()
        break
    except:
        print "Error..."
        SND_disconnected()