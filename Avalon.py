# -*- coding: utf-8 -*-
import random
from mygtts import *

HADNET = True   # 是否有網路(GTTS的下載)

### 遊戲參數
PlayerNumOfMission = { 5:[2,3,2,3,3] , 6:[2,3,4,3,4] , 7:[2,3,3,4,4] ,
                    8:[3,4,4,5,5] , 9:[3,4,4,5,5] , 10:[3,4,4,5,5] }
                    # Key:玩家人數 , Value:每回合出任務的人數
    # <注意> : 7人以上玩家，第4回合時需有兩張失敗卡，否則好人勝利。

NameOfCharacter = {'Merlin':u'梅林' , 'Percival':u'派西維爾' , 'Loyal Servant':u'亞瑟的忠臣' ,
                    'Oberon':u'奧伯倫' , 'Mordred':u'莫德雷德' , 'Assassin':u'刺客' , 'Morgana':u'魔甘娜' , 'Minion':u'莫德雷德的爪牙'}
                    # Key:角色英文名 , Value:角色中文名

CharacterList = ['Merlin' , 'Loyal Servant' , 'Mordred' , 'Assassin' , 'Loyal Servant',  # 五人遊玩
                'Loyal Servant',    # 六人 <if len(Character)>6: Character[5]取代為'Percival' >
                'Morgana',          # 七人
                'Loyal Servant',    # 八人
                'Loyal Servant',    # 九人
                'Oberon']           # 十人
                
class Avalon():
    def __init__(self):
        self.players_list = list()      # 所有玩家名稱清單 如: [name , name2] 順序為登入login.html的順序
        self.players_dict = dict()      # 玩家id所對應的玩家名稱 如: {name:id , name2:id2}
        self.players_count = 0          # 由admin()決定最大遊玩人數(5~10人)
        self.CARD_STATE = None          # [None , 'Binding'][0]
        self.tts_pls_put_card = dict()  # tts_pls_put_card={name:'1624908923' , name2:'1971327697'}
        self.tts_pls_assign_men = dict()
        ### 遊戲變數
        self.Players = list()           # <遊戲開始時>，亂數決定先後順序 : Players = players_list => random.shuffle(Players)
                                        # 第一順位為"領袖"，結束後換第二順位當"領袖"，而最後一位可獲得"湖中女神"卡片
                            
        self.Players2Character = dict() # {'name':'Merlin' , 'name2':'Mordred' , ...}
        self.Is_Started = False         # 是否已經(或正在)播放過<開始語音>以及<推播通知>(角色功能)
        self.Is_Alerted = dict.fromkeys(['bad_guy','merlin','loyalty','percival'],False)    # 是否已經(或正在)顯示Alert訊息
        self.Mission = []               # 任務成功與否(0:失敗,1:成功) len(Mission) = ROUND 如:[1,1,0,1]
        self.ROUND = 0                  # 當前出任務回合數(value:0~4)
        self.VOTE_COUNT = 0             # 紀錄已投票次數(最大值為5，達成時反派獲勝)
        self.Leader = 0                 # 當前"領袖"為Players[Leader] (Leader:0~players_count-1)
        self.MissionMen = []            # 此次任務領袖所決定的出任務玩家 如:[name , name2 , name3]
        self.OpposeMen = []             # 此次出任務的反對玩家
        self.Doing_Mission = {}         # {name:None , name2:True , name3:False} True代表成功，反之失敗(None為未選擇)
        self.Msg = {}                   # {name:{'Type':'bad_guy' , 'bad_guy_list':[] ,'msg':u'是你唯一的壞壞朋友喔。'} , name2:{'msg':u'是壞壞刺客。'...} ...}
        self.Is_Missioning = False      # 玩家是否正在出任務,可判別host/server頁面導入的html
        self.GameOver = None            # 'win' 或 'lose' 或 None
        self.GuessMerlin = None         # 刺客猜梅林:None還沒猜 True猜對 False猜錯
    def ResetVariable(self , all=True):
        # all=>True:全部重置 False:只重置遊戲變數
        self.Players = list()
        self.Players2Character = dict()
        self.Is_Started = False
        self.Is_Alerted = dict.fromkeys(['bad_guy','merlin','loyalty','percival'],False)
        self.Mission = []
        self.ROUND = 0
        self.VOTE_COUNT = 0
        self.Leader = 0
        self.MissionMen = []
        self.OpposeMen = []
        self.Doing_Mission = {}
        self.Msg = {}
        self.Is_Missioning = False
        self.GameOver = None
        self.GuessMerlin = None
        if(all):
            self.players_list = list()
            self.players_dict = dict()
            self.players_count = 0
            self.CARD_STATE = None
            self.tts_pls_put_card = dict()
            self.tts_pls_assign_men = dict()
            
    def SetGameVariable(self):
        # 遊戲開始前，決定角色及遊戲變數
        self.ResetVariable(all=False)
        for name in self.players_list:
            self.Players.append(name)
        # 亂數決定領袖順序
        random.shuffle(self.Players)
        # 亂數決定角色分配
        Character = CharacterList[:self.players_count]
        if (len(Character)>6):
            print '\n【Avalon】-增加派西維爾及魔甘娜!\n'
            Character[5]='Percival'
        random.shuffle(Character)
        for index in range(self.players_count):
            # 玩家名稱對應角色名稱
            self.Players2Character[self.Players[index]] = Character[index]
        # 玩家名稱對應任務成功與否，True為成功False為失敗，None為未選擇
        for name in self.players_list:
            self.Doing_Mission[name] = None
            self.Msg[name] = None
    def NextRound(self , winner):
        # winner = 0 or 1 (失敗或成功)
        print '\n【Avalon】-NextRound Function!'
        self.Is_Missioning = False
        self.Mission.append(winner)
        self.ROUND += 1
        self.VOTE_COUNT = 0
        self.MissionMen = []
        self.OpposeMen = []
        for name in self.players_list:
            self.Doing_Mission[name] = None
        if(self.Mission.count(0)>=3):
            print '【Avalon】-NextRound Function : 失敗3次以上，遊戲結束。\n'
            self.GameOver = 'lose'
            ####GAME OVER####
            return True
        elif(self.Mission.count(1)>=3):
            print '【Avalon】-NextRound Function : 成功3次以上，遊戲結束。\n'
            self.GameOver = 'win'
            ####GAME OVER####
            return True
        self.next_leader()
        return False
        
    # Some Function
    def had_player(self , name):
        if(self.players_list.count(name)==0):
            return False
        return True
    def player_is_full(self):
        if(len(self.players_list) == self.players_count):
            return True
        return False
        
    def id2name(self , id):
        # playername = id2name(id)
        for pname in self.players_dict.keys():
            if self.players_dict[pname] == id:
                return pname
        return None
        
    def get_leadername(self):
        return self.Players[self.Leader]
        
    def next_leader(self):
        #Leader = 0~players_count-1
        print '\n【Avalon】- next_leader function called.'
        self.Leader = self.Leader + 1 if(self.Leader<self.players_count-1) else 0
        self.MissionMen = []
        self.OpposeMen = []
        
    def addplayer(self , name):
        if(self.player_is_full()):   #人數已滿
            return False
        # 人數未滿
        if(not self.had_player(name)):     # 暱稱未註冊過
            print '\n【Avalon】-增加玩家 : ' , name
            self.players_list.append(name)
            if(HADNET):
                ################需要有網際網路才能執行tts_saver################
                self.tts_pls_put_card[name] = tts_saver(u'請，'+name+u'，感應自己的卡片').name()
                self.tts_pls_assign_men[name] = tts_saver(u'請，'+name+u'，指派任務').name()
            if(len(self.players_list) == self.players_count):
                print "\n【Avalon】-玩家人數已滿，可開始進行遊戲。\n"
            return True
        # 已註冊過的暱稱
        print '\n【Avalon】玩家登入:' , name , '\n'
        return False
        
        
        
        
        
        
        
        
        
        
        
        
        
        