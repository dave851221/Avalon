//======================================================================
//語音函式
var soundEmbed = null;
//======================================================================
function SoundPlay(filename){
    if (!soundEmbed){
        soundEmbed = document.createElement("embed");
        soundEmbed.setAttribute("src", "/sound/"+filename+".mp3");
        soundEmbed.setAttribute("hidden", true);
        soundEmbed.setAttribute("autostart", true);
    }
    else{
        document.body.removeChild(soundEmbed);
        soundEmbed.removed = true;
        soundEmbed = null;
        soundEmbed = document.createElement("embed");
        soundEmbed.setAttribute("src", "/sound/"+filename+".mp3");
        soundEmbed.setAttribute("hidden", true);
        soundEmbed.setAttribute("autostart", true);
    }
    soundEmbed.removed = false;
    document.body.appendChild(soundEmbed);
}
function ConfirmAll_ed0(){      // 播放遊戲開始的語音，並使webserver.py推播個人通知至每個玩家裝置
    //請莫德雷德、刺客確認彼此身分
    var d = 5500
    socket.emit('AlertToClient', {msg:'bad_guy'});
    SoundPlay('confirm_identity_ed0');
    console.log('請莫德雷德、刺客確認彼此身分');
    $('#gtts').html('請莫德雷德、刺客確認彼此身分');
    $('#gtts').fadeIn(500);
    
    setTimeout(function() {
        //請梅林確認壞人身分
        d = 5000
        socket.emit('AlertToClient', {msg:'merlin'});
        SoundPlay('Merlin');
        console.log('請梅林確認壞人身分');
        $('#gtts').html('請梅林確認壞人身分');
        
        setTimeout(function() {
            //請亞瑟的忠臣宣誓效忠
            d = 5000
            socket.emit('AlertToClient', {msg:'loyalty'});
            SoundPlay('loyalty');
            console.log('請亞瑟的忠臣宣誓效忠');
            $('#gtts').html('請亞瑟的忠臣宣誓效忠');
            
            setTimeout(function() {
                //遊戲開始
                d = 3000
                SoundPlay('gamestart');
                console.log('遊戲開始');
                $('#gtts').html('遊戲開始');
                socket.emit('ServerVariable', {variable:'Is_Started'});    //通知webserver.py，令其不再播放開始語音。
                setTimeout(function() {
                    $('#gtts').html('');
                    $('#gtts').hide();
                    window.location.reload(true);
                }, d);
                
            }, d);
            
        }, d);
        
    }, d);
}
function ConfirmAll_ed1(){      // 播放遊戲開始的語音，並使webserver.py推播個人通知至每個玩家裝置
    //請莫德雷德、刺客、魔甘娜確認彼此身分
    var d = 6500
    socket.emit('AlertToClient', {msg:'bad_guy'});
    SoundPlay('confirm_identity_ed1');
    console.log('請莫德雷德、刺客、魔甘娜確認彼此身分');
    $('#gtts').html('請莫德雷德、刺客、魔甘娜確認彼此身分');
    $('#gtts').fadeIn(500);
    
    setTimeout(function() {
        //請梅林確認壞人身分
        d = 5000
        socket.emit('AlertToClient', {msg:'merlin'});
        SoundPlay('Merlin');
        console.log('請梅林確認壞人身分');
        $('#gtts').html('請梅林確認壞人身分');
        
        setTimeout(function() {
            //請派西維爾確認梅林及魔甘娜身分
            d = 6000
            socket.emit('AlertToClient', {msg:'percival'});
            SoundPlay('Percival');
            console.log('請派西維爾確認梅林及魔甘娜身分');
            $('#gtts').html('請派西維爾確認梅林及魔甘娜身分');
            
            setTimeout(function() {
                //請亞瑟的忠臣宣誓效忠
                d = 5000
                socket.emit('AlertToClient', {msg:'loyalty'});
                SoundPlay('loyalty');
                console.log('請亞瑟的忠臣宣誓效忠');
                $('#gtts').html('請亞瑟的忠臣宣誓效忠');
                
                setTimeout(function() {
                    //遊戲開始
                    d = 3000
                    SoundPlay('gamestart');
                    console.log('遊戲開始');
                    $('#gtts').html('遊戲開始');
                    socket.emit('ServerVariable', {variable:'Is_Started'});    //通知webserver.py，令其不再播放開始語音。
                    setTimeout(function() {
                        $('#gtts').html('');
                        $('#gtts').hide();
                        window.location.reload(true);
                    }, d);
                    
                }, d);
                
            }, d);
            
        }, d);
        
    }, d);
}