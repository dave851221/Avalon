# 阿瓦隆電子桌遊
利用樹莓派、RC522、PL-IRM0101-X、Buzzer，建立阿瓦隆伺服器。

## 硬體需求
* RPi x1 or x2
* RC522 x1
* 蜂鳴器 x1
* 紅外線感應器 x1
* 紅外線遙控器 x1
* 線材及電阻

## 樹莓派準備作業

### 1. 軟體設定：

* 安裝Flask

        sudo pip install Flask

* 安裝Flask-socketio (安裝後reboot)

        sudo pip install eventlet
        sudo pip install flask-socketio

* 安裝[gTTS][1]

        sudo pip install gTTS

* 安裝[SPI-Py][3]
    
        git clone https://github.com/lthiery/SPI-Py.git
        cd SPI-Py
        sudo python setup.py install
        
* 開啟SPI介面(RC522需要)

        sudo raspi-config
        
* 安裝紅外線函式庫

    [IR-receiver][2]
    
    **註:"lircd.conf"及"lircrc"需改為"./lib/IR_black_remote/"內的檔案**
    
### 2. 接線：

* **RC522:**

    RC522 | Pin# | Pin name
    ---- | ---- | ----
    VCC | 17 | 3.3v
    RST | 22 | GPIO25
    GND | 20 | GND
    MISO| 21 | GPIO9
    MOSI| 19 | GPIO10
    SCK | 23 | GPIO11
    NSS/SDA | 24 | GPIO8
    IRQ | None| None

* **Buzzer:** GPIO 18 (Pin#12)
* **PL-IRM0101-X:** GPIO 2 (Pin#3)

## 下載並運行

### 1. 開啟樹莓派:

* 開啟網頁伺服器(伺服端)

        sudo python webserver.py

* 開啟讀卡機及紅外線偵測(客戶端)


    * 設定伺服器ip (網頁伺服器的ip , default：127.0.0.1) 
    
            python ipconfig.py

    * 開啟讀卡機

            python cardreader.py

    * 開啟紅外線偵測

            python IRreader.py

### 2. 平板或電腦開啟網頁(需要RPi Server的IP):

* 網址(進入後將可選擇遊玩人數)：`http://RPi's IP address/admin`

### 3. 玩家手機(需要RPi Server的IP):

* 網址(進入後將可輸入暱稱)：`http://RPi's IP address`

## RPi開機自動執行python(按照個人需求)

* 1.編輯"/etc/rc.local"檔案

        sudo nano /etc/rc.local
        
* 2.在"exit 0"前面加上(gTTS的檔案下載為相對路徑，所以cd指令是必要的):
    
        cd /home/pi/Desktop/Avalon
        sudo python webserver.py &

[1]: https://github.com/pndurette/gTTS
[2]: https://gitlab.com/dave851221/IR-receiver
[3]: https://github.com/lthiery/SPI-Py