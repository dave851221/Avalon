# -*- coding: utf8 -*-
import gtts
import os
import threading

def GTTS_save(Text):
    if not os.path.isfile("./static/sound/tts/%s.mp3"%Text):
        tts = gtts.tts.gTTS(text=Text.decode('big5'), lang="zh-tw")
        #tts = gtts.tts.gTTS(text=Text.decode('big5'), lang="en")
        tts.save("./static/sound/tts/%s.mp3"%Text)
        
class tts_saver(object):
    def __init__(self , text):
        import os
        import threading
        self.text = text
        self.file_name = './static/sound/tts/%s.mp3'%(lambda s: str(abs(hash(s)) % (10 ** 10)))(text)
        self.__thr_sav = None
        if not os.path.exists( self.file_name ):
            self.__thr_sav = threading.Thread(target=self.__thrd_saving , args=(text , self.file_name))
            self.__thr_sav.start()
            
    def __thrd_saving(self , txt , file_name):
        mytts = gtts.gTTS(text =txt,lang="zh-tw")
        mytts.save(file_name)
        
    def __ifsaved(self):
        return (not self.__thr_sav.isAlive()) if self.__thr_sav else True
        
    def name(self):
        return (lambda s: str(abs(hash(s)) % (10 ** 10)))(self.text)
    
