# -*- coding: utf-8 -*-
import random
import threading
from Avalon import *
from flask import *
from flask_socketio import *
from mygtts import *

app = Flask(__name__)
app.config['SECRET_KEY'] = 'secret!'
app.config['DEBUG'] = False
socketio = SocketIO(app)    # flask_socketio
avalon = Avalon()           # Game class
IR_permission = False
    
@app.route("/auto/reset")   # 重置遊戲
def auto_reset():
    global avalon
    avalon = Avalon()
    emit('ReStart',{},namespace='/player' , broadcast=True)
    return redirect(url_for("admin"))

@app.route("/auto/restart") # 重新開始
def auto_restart():
    global avalon
    avalon.ResetVariable(all=False)
    ChangeCARDSTATE({'state':'Binding'})
    emit('ReStart',{},namespace='/player' , broadcast=True)
    return redirect(url_for("admin"))

#####玩家端#####
# 登入頁面(login.html)
@app.route('/' , methods=['POST', 'GET'])
@app.route("/index", methods=['POST', 'GET'])
def index():
    global avalon
    is_full = avalon.player_is_full()
    if(request.method == 'POST'):   # 在暱稱輸入頁面POST回來
        if(request.form['playername']):
            name = request.form['playername']
            add_success = avalon.addplayer(name)
            if(add_success):    # 玩家註冊成功
                emit('Player_in' , {'name': name} , namespace='/admin' , broadcast=True )
                return redirect(url_for('GameStart',name=name))
            else:               # 玩家登入
                if(avalon.players_list.count(name)!=0): # 成功
                    return redirect(url_for('GameStart',name=name))
                return render_template('login.html' , is_full = is_full , login_error = True)
    return render_template('login.html' , is_full = is_full)

# 遊戲開始(顯示自己身分)
@app.route("/GameStart/<name>")
def GameStart(name):
    global avalon
    if(not avalon.had_player(name)):        # 尚未註冊的玩家
        return redirect(url_for("index"))
    elif(avalon.players_dict.has_key(name)):# 已註冊過卡片的玩家
        nameindex = avalon.players_list.index(name)
        id = avalon.players_dict[name]
        character = avalon.Players2Character[name]
        cname = NameOfCharacter[avalon.Players2Character[name]]
        character_msg = avalon.Msg[name]
        if(avalon.Is_Missioning and avalon.MissionMen.count(name)!=0 and avalon.Doing_Mission[name]==None):
            return render_template('client.html' , pname=name , nameindex=nameindex , id=id ,
                                    character=character , name=cname , character_msg=character_msg ,missioning=True)
        else:
            if(character=='Assassin' and avalon.GameOver=='win' and avalon.GuessMerlin==None):
                return render_template('client.html' , pname=name , nameindex=nameindex , id=id ,
                                        character=character , name=cname , character_msg=character_msg , guess=True , plist=avalon.players_list)
            return render_template('client.html' , pname=name , nameindex=nameindex , id=id ,
                                    character=character , name=cname , character_msg=character_msg)
    else:   # 已經登入過，但是尚未綁定卡片的玩家
        nameindex = avalon.players_list.index(name)
        return render_template('client.html' , pname=name , nameindex=nameindex , wait_for_bind=True)

#####伺服端#####
# 管理員設定人數及顯示訊息(admin.html , server_login.html)
@app.route("/admin", methods=['POST', 'GET'])
def admin():
    global avalon
    if(request.method == 'POST' and request.form['playercount']):
        pcount = int(request.form['playercount'])
        avalon.ResetVariable()
        avalon.players_count = pcount
        emit('players_count_isset',{},namespace='/admin_log' , broadcast=True)
        print "\n【主機】-決定人數: " + str(pcount)
        return render_template('server_login.html' , count = pcount)
    elif(avalon.players_count!=0):  # 已有裝置進入主機螢幕
        print "\n【主機】-登入成功\n"
        t = list()
        for p in avalon.players_list:
            t.append(avalon.tts_pls_put_card[p])
        count = avalon.players_count
        plist = avalon.players_list
        pdict = avalon.players_dict
        cstate = avalon.CARD_STATE
        return render_template('server_login.html' , count = count , plist = plist , pdict = pdict , cstate = cstate,
                                tts_put_card_list = t)
    else:   # 第一次進入主機模式
        return render_template('admin.html')

@app.route("/server")
def server():
    global  avalon
    try:
        count = avalon.players_count
        plist = avalon.players_list
        pdict = avalon.players_dict
        ROUND = avalon.ROUND
        VOTE_COUNT = avalon.VOTE_COUNT
        Leader = avalon.get_leadername()
        mlist = avalon.MissionMen
        olist = avalon.OpposeMen
        
        if(not avalon.Is_Started):      # 遊戲還未開始(需播放語音)
            return render_template('server_gtts.html' , count = count , plist = plist , pdict = pdict)

        elif(avalon.Is_Missioning):     # 玩家正在出任務
            return render_template('missioning.html' , ROUND = ROUND , mlist = mlist , Doing_Mission = avalon.Doing_Mission)
            
        elif(avalon.GameOver!=None):    # 遊戲已經結束
            result = {'win':True,'lose':False}[avalon.GameOver]
            if(result):
                if(avalon.GuessMerlin==None):   # 刺客還沒猜
                    for name in avalon.Players2Character:
                        if(avalon.Players2Character[name]=='Assassin'):
                            room = str('player_room_' + str(avalon.players_list.index(name)))
                    emit('Guess',{},namespace='/player' , room = room)
                    return render_template('gameover.html' , result = result , guess = True)
                elif(avalon.GuessMerlin):   # 猜對
                    result = False
                else:   # 猜錯
                    result = True
            #(elif not result or avalon.GuessMerlin!=None)
            showlist = getallplayers()
            return render_template('gameover.html' , result = result , showlist = showlist)
        #(else)
        tts_dict = avalon.tts_pls_assign_men
        numofmission = PlayerNumOfMission[avalon.players_count][avalon.ROUND]
        opposing = False
        if(avalon.CARD_STATE == 'OpposeMission'):
            opposing = True
        return render_template('server.html' , count = count , plist = plist , pdict = pdict ,
                                        ROUND = ROUND , VOTE_COUNT = VOTE_COUNT , Leader = Leader ,
                                        mlist = mlist , olist = olist , numofmission = numofmission ,
                                        tts_pls_assign_men = tts_dict , opposing = opposing , 
                                        mission = avalon.Mission)
    except Exception as e:
        print '\n\n\n'
        print e
        return str(e) , '\n\n\n'
        
    return ''

@app.route('/sound/<path:filename>')
def sound(filename):
    return send_from_directory('./static/sound', filename)
    
@app.route('/gtts/<path:filename>')
def sound_gtts(filename):
    return send_from_directory('./static/sound/tts', filename)
    
@app.route('/imgs/<path:filename>')
def image(filename):
    return send_from_directory('./static/img', filename)

@app.route('/javascript/<path:filename>')
def js(filename):
    return send_from_directory('./templates', filename)
    
#####對外API#####
# 卡片偵測(cardreader.py)一直都可以surf到card_detected，
# 但是要透過CARD_STATE判別此時此刻卡片的功用
@app.route('/card_detected/<id>')
def card_detected(id):
    global avalon
    print '\n【卡片偵測】偵測到卡片... id : ' + str(id)
    if(not avalon.CARD_STATE=='Binding' and not (id in avalon.players_dict.values())):
        print '【卡片偵測】未註冊的卡片...\n'
        return ''
    if(avalon.CARD_STATE): # 利用此變數來允許卡片動作
        if(avalon.CARD_STATE=='Binding'):  # 綁定卡片id及玩家名稱
            if(avalon.players_dict.values().count(id)==0): # 此id是否尚未被其他玩家使用
                print '\nBinding...'
                print '暱稱 :',avalon.players_list[len(avalon.players_dict.keys())]
                print 'Card ID :',id,'\n'
                room = str('player_room_'+str(len(avalon.players_dict.keys())))
                avalon.players_dict[avalon.players_list[len(avalon.players_dict.keys())]] = str(id)
                emit('BindPlayerID',{'id': id},namespace='/admin' , broadcast=True)
                
                pname = avalon.id2name(id) # 玩家暱稱
                pcharacter =  avalon.Players2Character[avalon.id2name(id)]    # 玩家的角色名稱(英文)
                pcharacter_cht = NameOfCharacter[avalon.Players2Character[avalon.id2name(id)]]
                emit('Binding_show_character',{'id': id , 'character':pcharacter , 'name':pcharacter_cht },namespace='/player' , room = room)
                print '\n【玩家端】-顯示- 玩家:',pname,'身分:',pcharacter_cht,'\n'
            else:
                for pname in avalon.players_dict.keys():
                    if(avalon.players_dict[pname]==id):
                        print u'此卡片已經被' + pname + u'使用!'
                        emit('UsedID',{'name':pname},namespace='/admin' , broadcast=True)
            if(len(avalon.players_dict)==avalon.players_count):
                ChangeCARDSTATE({'state':'BindingOver'})
                return ''
            return 'read_other'
        elif(avalon.CARD_STATE=='AssignMission'):   # 指派任務中
            pname = avalon.id2name(id)
            is_full = len(avalon.MissionMen) == PlayerNumOfMission[avalon.players_count][avalon.ROUND]
            if(is_full):
                print '\n【任務】-人數已滿'
                print '【任務】-暱稱 :',pname,'無法加入'
                return ''   # 回傳空字串使cardreader延遲一秒
            if(avalon.MissionMen.count(pname)==0):
                print '\n【任務】-指派'
                print '【任務】-暱稱 :',pname
                avalon.MissionMen.append(pname)
                emit('AddMissionMen',{'name':pname},namespace='/admin' , broadcast=True)
                is_full = len(avalon.MissionMen)==PlayerNumOfMission[avalon.players_count][avalon.ROUND]
                if(is_full):
                    print '\n【任務】-人數已滿，等待隊長確認'
                    emit('LeaderCheck',{},namespace='/admin' , broadcast=True)
                return 'Successful' # return給cardreader.py
            return ''   # 任務玩家重複感應
        elif(avalon.CARD_STATE=='OpposeMission'):   # 反對出任務的玩家
            pname = avalon.id2name(id)
            if(avalon.OpposeMen.count(pname)==0):
                print '\n【任務】-反對'
                print '【任務】-暱稱 :',pname
                avalon.OpposeMen.append(pname)
                emit('AddOpposeMen',{'name':pname},namespace='/admin' , broadcast=True)
                return 'Successful'
            return ''   # 反對玩家重複感應
    else:
        print '\n【卡片偵測】現在不需要卡片...'
        return ''

@app.route('/ir_detected/<code>')
def ir_detected(code):
    global avalon , IR_permission
    print '\n【紅外線】偵測:',code
    if(IR_permission):
        if(not len(avalon.players_dict)==avalon.players_count): #未綁定完成
            if(code == 'POWER'): ### 遊戲開始
                print '【紅外線】-遊戲開始'
                ChangeCARDSTATE({'state':'Binding'})    # 含ServerVariable({'variable':'IR_permission','value':False})
        elif(avalon.CARD_STATE == 'AssignMission'):    
            is_full = len(avalon.MissionMen) == PlayerNumOfMission[avalon.players_count][avalon.ROUND]
            if(code == 'CLOCK'): ### 重置出任務的玩家
                print '【紅外線】-重置出任務的玩家'
                ResetMisson({})
            if(is_full and code == 'EQ'):   ### 隊長確認
                print '【紅外線】-隊長確認'
                ChangeCARDSTATE({'state': 'OpposeMission'}) # 為了讓頁面重整後顯示反對訊息，所以不需要延遲
                emit('StartOppose',{},namespace='/admin' , broadcast=True)
        elif(avalon.CARD_STATE == 'OpposeMission'):    
            if(code == 'CLOCK'):    ### 重置反對的玩家
                print '【紅外線】-重置反對的玩家'
                ResetOppose({})
            elif(code == 'EQ'):     ### 決定反對的玩家
                print '【紅外線】-反對結束'
                OpposeFinish({})
        elif(not avalon.CARD_STATE):### 目前用來更新missioning.html
            print '【紅外線】-確認任務結果'
            emit('Reload',{},namespace='/admin' , broadcast=True)
            ServerVariable({'variable':'IR_permission','value':False})
            ChangeCARDSTATE({'state':'AssignMission'})  # 開放卡片感應
    else:
        print '【紅外線】權限未開啟'
    return ''

### function ###
def getallplayers():
    global avalon
    showlist = list()
    for name in avalon.Players2Character:
        if(avalon.Players2Character[name]=='Merlin'):
            showlist.append(u'梅林 : ' + name)
    for name in avalon.Players2Character:
        if(avalon.Players2Character[name]=='Percival'):
            showlist.append(u'派西維爾 : ' + name)
    msg = u'亞瑟的忠臣:'
    for name in avalon.Players2Character:
        if(avalon.Players2Character[name]=='Loyal Servant'):
            if(msg==u'亞瑟的忠臣:'):
                msg += name
            else:
                msg = msg + u'、' + name
    showlist.append(msg)
    for name in avalon.Players2Character:
        if(avalon.Players2Character[name]=='Oberon'):
            showlist.append(u'奧伯倫 : ' + name)
    for name in avalon.Players2Character:
        if(avalon.Players2Character[name]=='Mordred'):
            showlist.append(u'莫德雷德 : ' + name)
    for name in avalon.Players2Character:
        if(avalon.Players2Character[name]=='Assassin'):
            showlist.append(u'刺客 : ' + name)
    for name in avalon.Players2Character:
        if(avalon.Players2Character[name]=='Morgana'):
            showlist.append(u'魔甘娜 : ' + name)
    msg = ''
    for name in avalon.Players2Character:
        if(avalon.Players2Character[name]=='Minion'):
            if(msg==''):
                msg = name
            else:
                msg = msg + u'、' + name
    if(msg!=''):
        showlist.append(u'莫德雷德的爪牙 : ' + msg)
    return showlist

#####Websocket#####
@socketio.on('ChangeCARDSTATE', namespace='/admin')     ### 改變讀卡機狀態
def ChangeCARDSTATE(message):   
    global avalon
    if(message['state']=='Binding'):    # /admin頁面按下"開始遊戲按鈕" or 紅外線遙控
        print '【CARD_STATE】:卡片綁定開始。'
        avalon.CARD_STATE = 'Binding'
        t = list()  # gtts的檔名清單(包含玩家名稱)
        for p in avalon.players_list:
            t.append(avalon.tts_pls_put_card[p])
        emit('GoBind',{'tts_list':t},namespace='/admin' , broadcast=True)
        avalon.SetGameVariable()
        ServerVariable({'variable':'IR_permission','value':False})
        print '【角色分配完成】'
    elif(message['state']=='BindingOver'):
        if(avalon.CARD_STATE == 'Binding'):
            avalon.CARD_STATE = None
            print '【CARD_STATE】:卡片綁定結束。'
    elif(message['state']=='AssignMission' or message['state']=='OpposeMission'):    # 指派任務(感應到的卡片為要出任務的玩家)
        avalon.CARD_STATE = message['state']
        ServerVariable({'variable':'IR_permission','value':True})
    else:
        avalon.CARD_STATE = None
    print "【CARD_STATE】CARD_STATE is setted to :" , avalon.CARD_STATE
    
@socketio.on('ServerVariable', namespace='/admin')      ### 改變ServerVariable
def ServerVariable(message):
    global avalon , IR_permission
    if(message['variable']=='Is_Started'):  # 須注意:若開雙伺服器時，會收到兩次此訊息
        if(not avalon.Is_Started):  ## 第一次遊戲開始
            print "【語音】- 開場語音已播放完畢。"
            print "【webserver.py】avalon.Is_Started = True"
            avalon.Is_Started = True
            ChangeCARDSTATE({'state':'AssignMission'})  # 開放卡片感應
            
        else:
            print "【語音】- 副伺服器語音同步撥放完畢。"
    elif(message['variable']=='IR_permission'): # Client:socket.emit('ServerVariable', {variable:'IR_permission',value:true});
        if(message['value']):
            IR_permission = True
            print "【紅外線遙控】開放遙控權限"
        else:
            IR_permission = False
            print "【紅外線遙控】關閉遙控權限"

@socketio.on('AlertToClient', namespace='/admin')       ### 發送alert訊息至client端，須注意:若開雙伺服器時，會收到兩次此訊息
def AlertToClient(message):
    global avalon
    # avalon.Is_Alerted = {'bad_guy':False , 'merlin':False , 'loyalty':False , 'percival':False}
    if(message['msg']=='bad_guy' and not avalon.Is_Alerted['bad_guy']):    # 壞人群接收到的訊息
        avalon.Is_Alerted['bad_guy'] = True
        bad_guy_list = list()
        for player in avalon.players_list:     # 這邊無法判斷每個玩家身分，要在client判斷不輸出自己
            p = avalon.Players2Character[player]
            if(p=='Mordred' or p=='Assassin' or p=='Morgana' or p=='Minion'):
                bad_guy_list.append(player)
        if(len(bad_guy_list)==2):
            msg = u'是你唯一的壞壞朋友喔。'
        else:
            msg = u'是你的壞壞朋友們。'
        # 綁定Msg
        for player in bad_guy_list:
            avalon.Msg[player] = {'type':'bad_guy' , 'bad_guy_list':bad_guy_list , 'msg':msg }
        # 顯示Msg
        emit('AlertMsg',{'type':'bad_guy' , 'bad_guy_list':bad_guy_list , 'msg':msg },namespace='/player' , room = 'bad_guy')
        print '【推播訊息】room : bad_guy , bad_guy_list :' , bad_guy_list , ' , msg : ' , msg
        
    elif(message['msg']=='merlin' and not avalon.Is_Alerted['merlin']):    # 梅林接收到的訊息
        avalon.Is_Alerted['merlin'] = True
        bad_guy_list = list()
        for player in avalon.players_list:
            p = avalon.Players2Character[player]
            if(p=='Assassin' or p=='Morgana' or p=='Oberon' or p=='Minion'):
                bad_guy_list.append(player)
        if(len(bad_guy_list)==1):
            msg = u'是壞壞刺客。'
        else:
            msg = u'是一堆壞壞們。'
            
        # 綁定Msg
        for player in avalon.players_list:
            p = avalon.Players2Character[player]
            if(p=='Merlin'):
                avalon.Msg[player] = {'type':'merlin' , 'bad_guy_list':bad_guy_list , 'msg':msg}
        # 顯示Msg
        emit('AlertMsg',{'type':'merlin' , 'bad_guy_list':bad_guy_list , 'msg':msg },namespace='/player' , room = 'merlin')
        print '【推播訊息】room : merlin , bad_guy_list :' , bad_guy_list , ' , msg : ' , msg
        
    elif(message['msg']=='loyalty' and not avalon.Is_Alerted['loyalty']):  # 忠臣接收到的訊息
        avalon.Is_Alerted['loyalty'] = True
        msg = u'我願意為了亞瑟付出我的一切。'
        # 綁定Msg
        for player in avalon.players_list:
            p = avalon.Players2Character[player]
            if(p=='Loyal Servant'):
                avalon.Msg[player] ={'type':'loyalty' , 'msg':msg}
        # 顯示Msg
        emit('AlertMsg',{'type':'loyalty' , 'msg':msg },namespace='/player' , room = 'loyalty')
        print '【推播訊息】room : loyalty  , msg : ' , msg
        
    elif(message['msg']=='percival' and not avalon.Is_Alerted['percival']):# 派西維爾接收到的訊息
        avalon.Is_Alerted['percival'] = True
        merlin_list = list()
        msg = u'其中一個是梅林，另外一個是壞壞魔甘娜。'
        for player in avalon.players_list:
            p = avalon.Players2Character[player]
            if(p=='Merlin' or p=='Morgana'):
                merlin_list.append(player)
        # 綁定Msg
        for player in avalon.players_list:
            p = avalon.Players2Character[player]
            if(p=='Percival'):
                avalon.Msg[player] = {'type':'percival' , 'merlin_list':merlin_list , 'msg':msg}
        emit('AlertMsg',{'type':'percival' , 'merlin_list':merlin_list , 'msg':msg },namespace='/player' , room = 'percival')
        print '【推播訊息】room : percival , merlin_list :' , merlin_list , ' ,msg : ' , msg

@socketio.on('ResetMission', namespace='/admin')        ### 主機按下"重新指派任務"按鈕(or紅外線遙控)
def ResetMisson(message):
    # server.html傳送的socket訊息:隊長欲更改出任務的人，按下重置按鈕
    global avalon
    avalon.MissionMen = list()
    emit('Reload',{},namespace='/admin' , broadcast=True)
    print '\n【任務】-重置此任務指派的成員，請隊長重新指派。'

@socketio.on('ResetOppose', namespace='/admin')         ### 主機按下"重置反對投票"按鈕(or紅外線遙控)
def ResetOppose(message):
    # server.html傳送的socket訊息:隊長欲更改出任務的人，按下重置按鈕
    global avalon
    avalon.OpposeMen = list()
    emit('Reload',{},namespace='/admin' , broadcast=True)
    print '\n【任務】-重置反對的玩家。'
    
@socketio.on('OpposeFinish', namespace='/admin')        ### 主機按下"確定反對投票"按鈕(or紅外線遙控)
def OpposeFinish(message):
    # server.html傳送的socket訊息:確認無其他反對玩家後，按下按鈕
    global avalon
    '''
    half = PlayerNumOfMission[avalon.players_count][avalon.ROUND] % 2
    half += PlayerNumOfMission[avalon.players_count][avalon.ROUND] / 2
    '''
    half = avalon.players_count % 2
    half += avalon.players_count / 2    # 總人數的半數
    if(len(avalon.OpposeMen) >= half):
        print '\n【任務】-反對人數過半，領導換人。'
        avalon.VOTE_COUNT += 1
        if(avalon.VOTE_COUNT >= 5):     # 投票次數已達五次
            gameover = avalon.NextRound(0)
            print '\n【webserver.py】-OpposeFinish func : 反對次數超過五次，任務失敗。'
            if(gameover):
                print '\n【webserver.py】-OpposeFinish func : 遊戲結束!!!'
                ChangeCARDSTATE({'state':None})
                ####GAME OVER####
                emit('Reload',{},namespace='/admin' , broadcast=True)
            else:
                ChangeCARDSTATE({'state':None})
                emit('StartMission',{'state':False},namespace='/admin' , broadcast=True)    # 伺服端將Reload
                ChangeCARDSTATE({'state':'AssignMission'})
        else:
            print '此回合投票次數(VOTE_COUNT): ',avalon.VOTE_COUNT,'\n'
            avalon.next_leader()
            ChangeCARDSTATE({'state':None})
            emit('StartMission',{'state':False},namespace='/admin' , broadcast=True)    # 伺服端將Reload
            ChangeCARDSTATE({'state':'AssignMission'})
        ##領導換人##
    else:
        print '\n【任務】-開始出任務!!'
        ChangeCARDSTATE({'state':None}) # 出任務時不可感應卡片
        avalon.Is_Missioning = True     # 利用此變數改變伺服端顯示。此次任務結束時須改為False(ClientPushMission函式)
        emit('StartMission',{'state':True},namespace='/admin' , broadcast=True) # 伺服端將Reload
        for man in avalon.MissionMen:
            room = str('player_room_' + str(avalon.players_list.index(man)))
            emit('StartMission',{},namespace='/player' , room = room) # 開始推播訊息=>出任務玩家的裝置端(client.html)'''

@socketio.on('ClientPushMission', namespace='/player')
def ClientPushMission(message):     #出任務的玩家決定任務成功與否
    global avalon
    print u'\n【任務】玩家:' + message['name'] , u'任務 : ' + {True:u'成功',False:u'失敗'}[message['result']]
    avalon.Doing_Mission[message['name']] = message['result']
    
    done = 0
    for key in avalon.Doing_Mission:
        if(avalon.Doing_Mission[key]!=None):
            done += 1
    if(done != PlayerNumOfMission[avalon.players_count][avalon.ROUND]):
        emit('Reload',{},namespace='/admin' , broadcast=True)       # missioning.html
        return  # 若還有人未選擇任務成功與否
    result = True
    fake = 0
    real = 0
    for key in avalon.Doing_Mission:
        if(avalon.Doing_Mission[key]==False):
            fake += 1
            if(avalon.players_count >= 7 and avalon.ROUND == 3):    # 7,8,9名玩家時，第4回合需要兩張失敗卡
                if(fake>1):
                    result = False
                continue
            result = False  # 5,6名玩家時，只要一個失敗就失敗
        elif(avalon.Doing_Mission[key]):
            real += 1
    
    print '\n【任務】全數玩家任務完成 結果:任務' + {True:'成功',False:'失敗'}[result]
    print '【任務】 失敗:'+str(fake) , '成功:'+str(real)
    gameover = avalon.NextRound({True:1,False:0}[result])
    if(gameover):
        print '【webserver.py】-ClientPushMission func : 遊戲結束!!!'
        ####GAME OVER####
        emit('Reload',{},namespace='/admin' , broadcast=True)   # missioning.html
    else:
        print '【webserver.py】-ClientPushMission func : 遊戲繼續!!!'
        #ChangeCARDSTATE({'state':'AssignMission'})  # 開放卡片感應
        ServerVariable({'variable':'IR_permission','value':True})
        emit('RoundOver',{'result':result , 'fake':fake , 'real':real },namespace='/admin' , broadcast=True)   # missioning.html

@socketio.on('Guess', namespace='/player')
def Guess(message):
    global avalon
    print '\n\n【猜梅林】選項:',message['Merlin']
    if(avalon.Players2Character[message['Merlin']] == 'Merlin'):
        print '【猜梅林】正確'
        avalon.GuessMerlin = True
    else:
        print '【猜梅林】錯誤'
        avalon.GuessMerlin = False
    emit('Reload',{},namespace='/admin' , broadcast=True)
        
@socketio.on('join', namespace='/player')
def join(message):
    index = int(message['room'])
    name = avalon.players_list[index]
    room = str('player_room_' + message['room'])
    print '【Join Room】:', name , 'join room:' , room
    join_room(room)
    
@socketio.on('join_character', namespace='/player')
def join_character(message):
    index = int(message['room'])
    name = avalon.players_list[index]
    character = avalon.Players2Character[name]
    if(character=='Mordred' or character=='Assassin' or character=='Morgana' or character=='Minion'):  # 彼此可以看到彼此
        join_room('bad_guy')
        print '【Join Room】:', name , 'join room:' , 'bad_guy'
    elif(character=='Merlin'):  # 梅林可以看到壞人
        join_room('merlin')
        print '【Join Room】:', name , 'join room:' , 'merlin'
    elif(character=='Percival'):# 派西維爾可以看到梅林跟魔甘娜
        join_room('percival')
        print '【Join Room】:', name , 'join room:' , 'percival'
    elif(character=='Loyal Servant'):   # 忠臣也需要按下確認以掩蔽身分
        join_room('loyalty')
        print '【Join Room】:', name , 'join room:' , 'loyalty'
    elif(character=='Oberon'):  # 讓奧伯倫自己在一個沒意義的房間
        join_room('oberon')
        print '【Join Room】:', name , 'join room:' , 'oberon'
    # 所有腳色都有加入到角色room內，因此在最後的勝利階段時，可以利用此room通知玩家獲勝。
    
try:
    socketio.run(app, port=80, host='0.0.0.0')
except:
    print '\n【結束程式】webserver.py\n'   
        
